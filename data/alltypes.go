// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package data

type DamageCode int

const (
	DamageLight    DamageCode = 1
	DamageModerate DamageCode = 3
	DamageSerious  DamageCode = 6
	DamageDeadly   DamageCode = 10
)

type SkillDef struct {
	Name            string
	LinkedAttribute AttributeName
}

type Skill struct {
	SkillDef
	Value              int
	IsAutoCalculated   bool
	AutoCalcMultiplier float32
}

type DicePool struct {
	Name    string
	Max     int
	Current int
}
