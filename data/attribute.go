// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package data

import "strings"

type AttributeName string

func (an AttributeName) String() string {
	switch an {
	case Body:
		return "Body"
	case Quickness:
		return "Quickness"
	case Strength:
		return "Strength"
	case Intelligence:
		return "Intelligence"
	case Willpower:
		return "Willpower"
	case Charisma:
		return "Charisma"
	case Reaction:
		return "Reaction"
	case Magic:
		return "Magic"
	default:
		return "UNKNOWN"
	}
}

func AttributeNameFromString(s string) (AttributeName, bool) {
	if strings.EqualFold(s, string(Magic)) {
		return Magic, true
	}

	for _, an := range DefaultAttributeNames {
		if strings.EqualFold(s, string(an)) {
			return an, true
		}
	}

	return "UNKNOWN", false
}

const (
	Body         AttributeName = "BOD"
	Quickness    AttributeName = "QUI"
	Strength     AttributeName = "STR"
	Intelligence AttributeName = "INT"
	Willpower    AttributeName = "WIL"
	Charisma     AttributeName = "CHA"
	Reaction     AttributeName = "REACT"
	Magic        AttributeName = "MAGIC"
)

var DefaultAttributeNames = []AttributeName{
	Strength, Quickness, Body, Intelligence, Willpower, Charisma, Reaction,
}

type Attribute struct {
	Name         AttributeName
	BaseValue    int
	RacialMod    int
	PrePoolMods  int
	PostPoolMods int
	TotalValue   int
}
