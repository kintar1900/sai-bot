// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package data

import "errors"

type Character struct {
	Id                string
	Name              string
	PlayerDiscordUser string
	Attributes        []Attribute
	Skills            []Skill
	Pools             map[string]DicePool
	Essence           float32
	BioIndex          float32
	StunDamage        int
	PhysicalDamage    int
	OverflowDamage    int
}

func NewCharacter(userId, charName string) Character {
	attribs := make([]Attribute, len(DefaultAttributeNames))
	for i, an := range DefaultAttributeNames {
		attribs[i] = Attribute{Name: an}
	}
	return Character{
		Id:         userId,
		Name:       charName,
		Attributes: attribs,
		Pools: map[string]DicePool{
			"COMBAT": {Name: "COMBAT"},
		},
		Essence: 6,
	}
}

type CharacterRepository interface {
	Save(userId string, characters []Character) error
	Load(userId string) ([]Character, error)
}

type InMemoryCharacterRepo struct {
	chars map[string][]Character
}

func NewInMemoryCharacterRepo() *InMemoryCharacterRepo {
	return &InMemoryCharacterRepo{
		chars: make(map[string][]Character),
	}
}

func (i *InMemoryCharacterRepo) Save(userId string, characters []Character) error {
	if i == nil {
		*i = *NewInMemoryCharacterRepo()
	}

	i.chars[userId] = characters

	return nil
}

func (i *InMemoryCharacterRepo) Load(userId string) ([]Character, error) {
	if chars, ok := i.chars[userId]; ok {
		return chars, nil
	}

	return nil, errors.New("no characters found")
}
