# SAI-bot
## Shadowlands Administrative Intelligence Bot

![current release](https://img.shields.io/badge/dynamic/json?color=blue&label=current%20release&query=%24.latestRelease&url=https%3A%2F%2Fgitlab.com%2Fkintar1900%2Fsai-bot%2F-%2Fjobs%2Fartifacts%2Fmain%2Fraw%2Fbadge-info.json%3Fjob%3DUnitTests&style=flat-square&logo=gitlab) ![GitLab pipeline](https://img.shields.io/gitlab/pipeline/kintar1900/sai-bot/main.svg?style=flat-square&logo=gitlab)

## Overview

SAI-Bot is a discord bot that provides dice-rolling capability for the third edition of Shadowrun.  I plan on expanding its capabilities over time.

Add our public bot to your server from [this link](https://discord.com/api/oauth2/authorize?client_id=788463172050092062&scope=bot&permissions=268594258)

## Usage

Sai-bot is intended to make character record keeping and dice rolling completely painless.  For now, ve has a fully-functional SR3 dice rolling system:

`!test <difficulty> <pool size> [description]` will roll `pool size` six-sided die, handle the "exploding" dice mechanic for you, and report the raw rolls, as well as whether the result was a critical failure (all 1's), a failure (nothing > `difficulty`) or a success, including the number of successes.

## Planned Features

* Character tracking
  * Attributes and skills
  * Initiative tracker
  * Pools, including maximum and current values and refresh on new combat rounds
  * Condition track
  * Weapons and Armor
  * Gear and ratings
  * Cyberware/Bioware and descriptions
  * Essence and Bio-index

## Self-Hosting

Self-hosting Sai-bot is easy by using [Docker](https://www.docker.com).  This project's [container registry](https://gitlab.com/kintar1900/sai-bot/container_registry) has the latest version of the docker image avaliable.  Once Docker is installed, simply run `docker run -it -e DISCORD_BOT_TOKEN=<your bot token here> registry.gitlab.com/kintar1900/sai-bot/bot:latest` to start the bot.

### Building

If you want to build sai-bot yourself, you'll need Go 1.15.x and Docker installed.
Running `make build` will build the docker file for a scratch-based linux container that will run the bot.
