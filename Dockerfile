FROM registry.gitlab.com/kintar1900/build-tools/go:1.15-alpine as BUILD

WORKDIR /bot
COPY . .

RUN apk update && apk --update add ca-certificates
RUN go mod download && go mod verify

RUN CGO_ENABLED=0 go build -a -o /bot/sai-bot -ldflags "-s -w -X main.version=$(git describe --tags --dirty)" cmd/main.go  && upx -q /bot/sai-bot

FROM scratch

WORKDIR /bot
COPY --from=BUILD /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=BUILD /bot/sai-bot .

CMD [ "/bot/sai-bot" ]
