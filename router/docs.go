// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/*
Package router defines an opinionated command router for use with discordgo.

For each message arriving on a channel the bot can see, the bot looks for a character sequence at the beginning of the message to determine
if it should treat the text as a command or not.  This is referred to as the Trigger.  If no Trigger is detected, no commands will be executed.

*/
package router
