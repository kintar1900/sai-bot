module gitlab.com/kintar1900/sai-bot

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/google/addlicense v0.0.0-20200906110928-a0294312aa76 // indirect
	gitlab.com/kintar1900/botamatic v0.2.1
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
)
