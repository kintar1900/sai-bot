// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package bot

import (
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/kintar1900/botamatic/router"
	"gitlab.com/kintar1900/sai-bot/commands"
	"gitlab.com/kintar1900/sai-bot/data"
	"log"
	"os"
	"os/signal"
	"reflect"
	"runtime"
	"strings"
	"syscall"
	"time"
)

type botConfig struct {
	DiscordToken string `env:"DISCORD_BOT_TOKEN"`
}

type MetricData struct {
	version   string
	startedAt time.Time
}

var ephemeralRepo data.CharacterRepository

func (cfg *botConfig) populate() error {
	if cfg == nil {
		*cfg = botConfig{}
	}

	v := reflect.ValueOf(cfg)
	t := reflect.TypeOf(cfg)
	missingVars := make([]string, 0)

	for f := 0; f < t.Elem().NumField(); f++ {
		fld := t.Elem().Field(f)
		envVar := fld.Tag.Get("env")
		val, ok := os.LookupEnv(envVar)
		if !ok {
			missingVars = append(missingVars, envVar)
		} else {
			v.Elem().Field(f).SetString(val)
		}
	}

	if len(missingVars) > 0 {
		return errors.New(fmt.Sprint("can't start: missing required environment variables: ", strings.Join(missingVars, ", ")))
	}

	if strings.Index("Bot ", cfg.DiscordToken) != 0 {
		cfg.DiscordToken = "Bot " + cfg.DiscordToken
	}

	return nil
}

func Run(version string) {
	cfg := botConfig{}
	if err := cfg.populate(); err != nil {
		log.Fatalln(err)
	}

	ephemeralRepo = data.NewInMemoryCharacterRepo()

	log.Println(cfg.DiscordToken)

	session, _ := discordgo.New()
	session.Token = cfg.DiscordToken

	if err := startDiscord(session); err != nil {
		log.Fatalln(err)
	}

	defer func() { _ = session.Close() }()

	printWelcomeMessage(version)
	startHandlers(session, version)

	sChan := make(chan os.Signal, 1)
	log.Println("Press CTRL+C to exit...")
	signal.Notify(sChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Kill, os.Interrupt)
	_ = <-sChan
}

func printWelcomeMessage(version string) {
	fmt.Printf(`
 ________  ________  ___                 ________  ________  _________   
|\   ____\|\   __  \|\  \               |\   __  \|\   __  \|\___   ___\ 
\ \  \___|\ \  \|\  \ \  \  ____________\ \  \|\ /\ \  \|\  \|___ \  \_| 
 \ \_____  \ \   __  \ \  \|\____________\ \   __  \ \  \\\  \   \ \  \  
  \|____|\  \ \  \ \  \ \  \|____________|\ \  \|\  \ \  \\\  \   \ \  \ 
    ____\_\  \ \__\ \__\ \__\              \ \_______\ \_______\   \ \__\
   |\_________\|__|\|__|\|__|               \|_______|\|_______|    \|__|
   \|_________|                             `)
	fmt.Printf("%29s\n\n", version)
}

func startDiscord(session *discordgo.Session) error {
	err := session.Open()
	if err != nil {
		return fmt.Errorf("failed to connect to discord: %w", err)
	}
	return nil
}

func startHandlers(session *discordgo.Session, version string) {
	metric := MetricData{
		version:   version,
		startedAt: time.Now(),
	}

	if metric.version == "" {
		metric.version = "Unknown"
	}

	r := router.New([]string{"!", "sai!"}, []router.Command{
		createStatusCommand(),
		commands.DiceRolling(),
	}, []router.ChainedExecutionHandler{
		func(ctx *router.ExecutionContext) (proceed bool) {
			ctx.Set("MetricData", &metric)
			ctx.Set("MemoryRepo", &ephemeralRepo)
			return true
		},
	})

	r.Start(session)
}

func createStatusCommand() router.Command {
	return router.Command{
		Name: "status",
		Aliases: []string{
			"stat",
		},
		Usage:       "status",
		Description: "Prints version and various metrics information",
		Handler:     handleStatusCommand,
	}
}

func handleStatusCommand(ctx *router.ExecutionContext) {
	var metrics MetricData
	rawMetric, ok := ctx.Get("MetricData")
	if ok {
		metricsPtr, ok := rawMetric.(*MetricData)
		if !ok {
			_ = ctx.Reply("I'm sorry, there appears to be a problem with my brain.  Talk to my programmer, please.")
			return
		}
		metrics = *metricsPtr
	}

	fields := make([]*discordgo.MessageEmbedField, 0)
	fields = append(fields, &discordgo.MessageEmbedField{
		Name:  "Version",
		Value: metrics.version,
	})

	uptime := time.Since(metrics.startedAt)
	uptimeStr := ""
	if uptime.Seconds() < 60 {
		uptimeStr = fmt.Sprintf("%.0f seconds", uptime.Seconds())
	} else if uptime.Minutes() < 60 {
		uptimeStr = fmt.Sprintf("%.2f minutes", uptime.Minutes())
	} else {
		uptimeStr = fmt.Sprintf("%.2f hours", uptime.Hours())
	}

	memStats := runtime.MemStats{}
	runtime.ReadMemStats(&memStats)

	embed := discordgo.MessageEmbed{
		Type:  discordgo.EmbedTypeRich,
		Title: "SAI-Bot Status",
		Color: 0x03adfc,
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Status as of %v", time.Now().Format("2006-01-02 15:04:05 MST")),
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "Version",
				Value:  metrics.version,
				Inline: true,
			},
			{
				Name:   "Uptime",
				Value:  uptimeStr,
				Inline: true,
			},
			{
				Name:   "Memory In Use",
				Value:  fmt.Sprintf("%.2fMB", float64(memStats.HeapAlloc)/1000000.0),
				Inline: true,
			},
		},
	}

	if err := ctx.ReplyEmbed(&embed); err != nil {
		log.Println(fmt.Sprintf("%v", err))
	}
}
