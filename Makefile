UPX := $(shell command -v upx 2> /dev/null)
AWSCLI := $(shell command -v aws 2> /dev/null)
DOCKER := $(shell command -v docker 2> /dev/null)

.PHONY: clean format

GOROOT := $(shell go env GOROOT)
GOPATH := $(shell go env GOPATH)
GO := $(GOROOT)/bin/go
export GOPATH
export GOROOT

VERSION := $(shell git describe --tags --dirty)

LATEST_RELEASE := $(shell git describe --tags $(shell git rev-list --tags --max-count=1))
MASTER_VERSION := $(shell git describe --tags)

clean:
	rm -rf dist/
	rm -rf build/

build: cmd/main.go
	@echo "Building bot $(VERSION)"
	GOOS="linux" GOARCH="amd64" $(GO) build -o build/sai-bot "gitlab.com/kintar1900/sai-bot/cmd"

# Compress the executable, if possible
ifndef UPX
	@echo "\033[1;93mWARNING:\033[0m upx is not available: executable will be larger than necessary"
else
	@echo "Compressing executable"
	upx -q build/sai-bot > /dev/null
endif
	@echo "Done building bot"

badges:
	echo "{\"latestRelease\": \"$(LATEST_RELEASE)\", \"masterVersion\": \"$(MASTER_VERSION)\"}" > badge-info.json
