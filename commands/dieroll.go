// SAI-Bot - A Discord Bot to aid running Shadowrun 3rd Edition games
// Copyright (C) 2020 Alec Lanter
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package commands

import (
	"fmt"
	"gitlab.com/kintar1900/botamatic/router"
	"log"
	"math/rand"
	"sort"
	"strings"
	"time"
)

func DiceRolling() router.Command {
	return router.Command{
		Name: "test",
		Aliases: []string{
			"roll",
		},
		Usage:       "test <difficulty number> <pool size> [test name]",
		Description: "Rolls a test at the given difficulty with the given number of dice",
		Handler:     handler,
	}
}

func handler(ctx *router.ExecutionContext) {
	argLen := len(ctx.Arguments)
	if argLen < 2 {
		_ = ctx.Reply("That command requires at least two arguments.")
		return
	}

	size, ok := ctx.Arguments[1].AsInt()
	if !ok {
		_ = ctx.Reply(fmt.Sprintf("I didn't recognize '%s' as a number", ctx.Arguments[1]))
	}

	desc := ""
	if argLen > 2 {
		words := make([]string, 0)
		for i := 2; i < argLen; i++ {
			words = append(words, ctx.Arguments[i].String())
		}
		desc = strings.Join(words, " ")
	}

	if ctx.Arguments[0].String() == "open" {
		doOpenTest(ctx, size, desc)
		return
	}

	diff, ok := ctx.Arguments[0].AsInt()
	if !ok {
		_ = ctx.Reply(fmt.Sprintf("I didn't recognize '%s' as a number", ctx.Arguments[0]))
	}

	result := rollDice(int(diff), int(size))

	response := strings.Builder{}
	response.WriteString(fmt.Sprintf("**%s rolls the dice**", ctx.Message.Author.Username))
	if desc != "" {
		response.WriteString(fmt.Sprintf(": `%s`", desc))
	}
	response.WriteString(fmt.Sprintf("\n**Difficulty** : %d", result.Difficulty))
	response.WriteString(fmt.Sprintf("\n**Rolls**: %s", result.RollsString()))
	response.WriteString("```diff\n")
	if result.Catastrophic() {
		response.WriteString("- CATASTROPHIC FAILURE!!!")
	} else {
		successes := result.Successes()
		if successes == 0 {
			response.WriteString("- FAILURE")
		} else {
			response.WriteString(fmt.Sprintf("+ SUCCESS! (x%d)", successes))
		}
	}
	response.WriteString("\n```\n")

	err := ctx.Reply(response.String())
	if err != nil {
		log.Println(err)
	}
}

func doOpenTest(ctx *router.ExecutionContext, size int64, desc string) {
	result := rollDice(0, int(size))

	response := strings.Builder{}
	response.WriteString(fmt.Sprintf("**%s rolls the dice**", ctx.Message.Author.Username))
	if desc != "" {
		response.WriteString(fmt.Sprintf(": `%s`", desc))
	}
	response.WriteString(fmt.Sprintf("\n**Rolls**: %s", result.RollsString()))
	response.WriteString("```diff\n")
	response.WriteString(fmt.Sprintf("+ RESULT : %d", result.Raw[len(result.Raw)-1]))
	response.WriteString("\n```\n")

	err := ctx.Reply(response.String())
	if err != nil {
		log.Println(err)
	}
}

type DiceResult struct {
	Raw        []int
	Difficulty int
}

func (r DiceResult) RollsString() string {
	rolls := make([]string, len(r.Raw))
	for i, v := range r.Raw {
		rolls[i] = fmt.Sprintf("%v", v)
	}
	return strings.Join(rolls, ", ")
}

func (r DiceResult) Successes() int {
	count := 0
	for _, v := range r.Raw {
		if v >= r.Difficulty {
			count++
		}
	}
	return count
}

func (r DiceResult) Catastrophic() bool {
	for _, v := range r.Raw {
		if v != 1 {
			return false
		}
	}
	return true
}

func rollDice(diff, pool int) DiceResult {
	result := DiceResult{
		Difficulty: diff,
	}
	results := make([]int, pool)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < pool; i++ {
		roll := 6
		for roll == 6 {
			roll = rand.Intn(6) + 1
			results[i] += roll
		}
	}

	sort.Ints(results)

	result.Raw = results

	return result
}
