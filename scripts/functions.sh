#!/usr/bin/env bash

BG_RED='\e[41m'
YLW='\e[93m'
NC='\e[39m\e[49m' # Reset color code

# returns 1 if the current working tree is clean, 0 if it is dirty
function is_tree_clean() {
  if [ "$(git status --short)" == "" ]; then
    reutrn 0
  else
    return 1
  fi
}

# Since gitlab is being weird about the refs 'develop' or 'master' being available, this functon does the following:
# * If CI_COMMIT_REF_NAME is present, use this as the branch name, otherwise parse the name from git
# * Use the branch name to determine final, rc, or milestone status
# * Build the version from the current tree
function build_version() {
  local ROOT_VER=$(git describe --tags --dirty)
  local BRANCH=${CI_COMMIT_REF_NAME:-$(git rev-parse --abbrev-ref HEAD)}

  local SEMVER=$(echo "${ROOT_VER}" | cut -d '-' -f 1)
  local SUBVER=$(echo "${ROOT_VER}" | cut -s -d '-' -f 2-)

  case "$BRANCH" in
  "main")
    if [ "" != $SUBVER ]; then
      echo -e "${BG_RED}REFUSING TO BUILD DIRTY MASTER${NC}"
      echo "You are building from the master branch, but the branch is ahead of the latest tag."
      echo "Please update your tags before attempting to build master."
      exit 2
    fi
    ;;
  "develop")
    echo "${SEMVER}-rc-${SUBVER}"
    ;;
  *)
    echo "${SEMVER}-milestone-${SUBVER}"
    ;;
  esac
}

# If given "master" or "develop" as an argument, builds the current version label for that branch
# Otherwise, builds the milestone of the current branch.
# In both cases, sends the result to stdout
function build_branch_version() {
  if [ "main" == "$1" ]; then
    # Calculate main version
    git describe --tags main
  elif [ "develop" == "$1" ]; then
    # Calculate develop version
    local DEV_VER=$(git describe --tags develop)
    local DEV_SUBVER=$(echo "${DEV_VER}" | cut -d '-' -f 2-)
    if [ "" != "${DEV_SUBVER}" ]; then
      if [ "${DEV_SUBVER}" != "${DEV_VER}" ]; then
        local DEV_VER=$(echo "${DEV_VER}" | cut -d '-' -f 1)-rc.${DEV_SUBVER}
      else
        local DEV_VER=$(echo "${DEV_VER}" | cut -d '-' -f 1)-rc.0
      fi
    fi
    echo "${DEV_VER}"
  else
    local VER=$(git describe --tags --dirty)
    local SUBVER=$(echo "${VER}" | cut -d '-' -f 2-)
    if [ "" != "${SUBVER}" ] && [ "${VER}" != "${SUBVER}" ]; then
      VER=$(echo "${VER}" | cut -d '-' -f 1)-milestone-${SUBVER}
    fi
    echo "${VER}"
  fi
}
