#!/usr/bin/env bash

# Versioning goes like this:
#
# * Calculate the current version of master with `git describe --tags master`.
#   * If master is not a tagged commit, break on '-', and use the remainder as a 'patch' commit
# * Calculate the current version of develop with `git describe --tags develop`.
#   * Break on the first '-', and build a release candidate version using 'rc.'
# * Increment the minor version segment of develop and build a milestone version for the current branch.

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd "$DIR" || exit 1

. functions.sh || exit 1

cd - > /dev/null || exit 1

# Calculate master version
MASTER_VER=$(build_branch_version "master" | cut -d '-' -f 1-2)
DEV_VER=$(build_branch_version "develop" | cut -d '-' -f 1-2)
LOCAL_VERSION=$(build_branch_version)

echo -e "${YLW}Master:${NC} ${MASTER_VER}"
echo -e "${YLW}Develop:${NC} ${DEV_VER}"
echo -e "${YLW}Local:${NC} ${LOCAL_VERSION}"

# Update README with master
BADGE="!\\[Release Badge\\]\\(https:\\/\\/img.shields.io\\/static\\/v1\\?label=go.mod\\&message=${MASTER_VER}\\&color=blue\\)"
REPLACE="s/!\\[Release Badge\\].*/${BADGE}/g"
sed -i -e "$REPLACE" "$DIR"/../README.md

# Update README with develop
BADGE="!\\[Dev Badge\\]\\(https:\\/\\/img.shields.io\\/static\\/v1\\?label=go.mod\\&message=${DEV_VER}\\&color=blue\\)"
REPLACE="s/!\\[Dev Badge\\].*/${BADGE}/g"
sed -i -e "$REPLACE" "$DIR"/../README.md

# Update beacon manifest file
ManifestVersion=$(echo "${LOCAL_VERSION}" | cut -d '-' -f 1 | cut -d 'v' -f 2)
REPLACE="s/(assemblyIdentity version=\").*(\" processor.*)/\1${ManifestVersion}\2/g"
sed -i -re "$REPLACE" "$DIR"/../resources/beacon.manifest
