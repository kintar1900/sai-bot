#!/usr/bin/env bash

set -e

TARGET=gitlab.com/kintar1900/sai-bot/cmd
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

VERSION=$("$DIR"/parseVersion.sh)

LDFLAGS="-X main.version=${VERSION} -s -w"

set +e

upxBin="$(command -v upx)"

echo "Making directories...."

set -e

OUTDIR="build"
rm -rf "$OUTDIR"
mkdir -p $OUTDIR

echo "Building bot version ${VERSION}..."

OUTFILE="${OUTDIR}/sai-bot"

GOOS=${GOOS:-"linux"} GOARCH="amd64" go build -ldflags "${LDFLAGS}" -o "$OUTFILE" $TARGET

if [ -n "$upxBin" ]; then
  upx -q "${OUTFILE}"
fi
